import { Component } from '@angular/core';
import { NbSidebarService, NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  items: NbMenuItem[];
  constructor(
    private sidebarService: NbSidebarService
  ) {
    this.items = [
      { title: "Home", icon: "home" },
      { title: "About", icon: "sun" },
    ]
  }
  toggle() {
    this.sidebarService.toggle();
  }
}
